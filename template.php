<?php

function asesor_blue_regions() {
  return array(
       'header' => t('header'),
       'right' => t('right sidebar'),
       'submenu' => t('submenu'),
       'content' => t('content'),
       'footer' => t('footer')
  );
}

function phptemplate_search_theme_form($form) {
  /**
   * This snippet catches the default searchbox and looks for
   * search-theme-form.tpl.php file in the same folder
   * which has the new layout.
   */
  return _phptemplate_callback('search_theme_form', array('form' => $form), array('search-theme-form'));
}

/**
* Duplicate of theme_links, but adds active class to <li> elements.
*
* Return a themed set of links.
*
* @param $links
*   A keyed array of links to be themed.
* @param $attributes
*   A keyed array of attributes
* @return
*   A string containing an unordered list of links.
*/
function asesor_blue_links($links, $attributes = array('class' => 'links')) {
  $output = '';

  if (count($links) > 0) {
    $output = '<ul'. drupal_attributes($attributes) .'>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = $key;

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class .= ' first';
      }
      if ($i == $num_links) {
        $class .= ' last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && $_GET['q'] == 'node'))) {
        $class .= ' active';
      }
      $output .= '<li class="'. $class .'">';

      // Is the title HTML?
      $html = isset($link['html']) && $link['html'];

      // Initialize fragment and query variables.
      $link['query'] = isset($link['query']) ? $link['query'] : NULL;
      $link['fragment'] = isset($link['fragment']) ? $link['fragment'] : NULL;

      if (isset($link['href'])) {
        $output .= l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment'], FALSE, $html);
      }
      else if ($link['title']) {
        //Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (!$html) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}
