<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language; ?>" lang="<?php print $language; ?>">
<head>
<title><?php print $head_title; ?></title>
<?php print $head; ?><?php print $styles; ?>
<?php if(module_exists('javascript_aggregator')) {$scripts = javascript_aggregator_cache($scripts);}?>
</head>
<body>
<!--layout-->
<div id="header-region" class="clear-block"><?php print $header; ?></div>
  <div id="header">
    <div class="navi">
      <div class="logo">
      	<?php if ($logo) : ?>
        <strong><a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>"><img src="<?php print($logo); ?>" alt="<?php print t('Home'); ?>" /></a></strong>
      <?php endif; ?>
		<?php if (isset($primary_links)) : ?>
          <?php print theme('links', $primary_links, array('class' => 'lavaLamp')); ?>
        <?php endif; ?>
      </div>
      <div class="donde">
        <?php if (isset($secondary_links)) : ?>
          <?php print theme('links', $secondary_links); ?>
        <?php endif; ?>
      </div>
      <!--/donde -->
    </div>
    <div class="btop"></div>
    <div class="bann"></div>
    <div class="subbann">
		<ul>
        	<li><a href="javascript:void(0)">Other menu</a> <span>‘</span></li>
        	<li><a href="/asesor-blue-theme/blog/1">Blog</a> <span>‘</span></li>
        	<li><a href="/asesor-blue-theme/newsletters">Newsletters</a></li>
		</ul>
		<?php if ($search_box): ?>
        <div class="search">
          <?php print $search_box; ?>
        </div>
        <!--/search -->
        <?php endif; ?>
    </div>
    <div class="rebor"></div>
  </div>
<!--/hd -->
<div class="contentp">
    <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
    <?php if ($title): print '<h1'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h1>'; endif; ?>
    <?php if ($tabs): print $tabs .'</div>'; endif; ?>
    <?php if (isset($tabs2)): print $tabs2; endif; ?>
  <div class="subcontent">
 <div class="left">
    <?php if ($help): print $help; endif; ?>
    <?php if ($messages): print $messages; endif; ?>
    <?php print $content; ?> <?php print $feed_icons; ?>
    </div>
<div class="right">
  <div class="colder">
	<?php if ($sidebar_right): ?>
      <?php print $sidebar_right; ?>
	<?php endif; ?>
  </div>
</div>
<div class="clear"></div>
</div>
</div>
<div class="footer">
<div class="menuf">
<?php print $footer_message; ?>
<p><strong>Asesor Blue, a Drupal theme by <a href="http://www.maismedia.com">MaisMedia Optimizacion web</a> © 2010.</strong> <a href="javascript:void(0)">impressum</a> ‘ <a href="javascript:void(0)">terms of use</a>.</p>
</div>
<div class="newsform">
<p class="tit-bol">Business Name</p>
</div><div class="clear"></div></div>
<?php print $scripts; ?>
<?php print $closure; ?>
</body>
</html>
